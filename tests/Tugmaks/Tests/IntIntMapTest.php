<?php
/**
 * @author Maxim Tugaev <tugmaks@yandex.ru>
 */

declare(strict_types=1);

namespace Tugmaks\Tests;

use Generator;
use OutOfRangeException;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Shmop;
use Tugmaks\IntIntMap;
use function ftok;
use function intdiv;
use function shmop_delete;
use function shmop_open;
use function shmop_size;

final class IntIntMapTest extends TestCase
{
    private const MEM_SIZE = 2 * 1024 ** 3;

    private Shmop $shmop;

    protected function setUp(): void
    {
        $shmop = shmop_open(
            key: ftok(__FILE__, 'C'),
            mode: 'c',
            permissions: 0644,
            size: self::MEM_SIZE
        );

        if (false === $shmop) {
            throw new RuntimeException('Unable to open shared memory');
        }

        $this->shmop = $shmop;
    }

    protected function tearDown(): void
    {
        shmop_delete($this->shmop);
    }

    /**
     * @test
     * @dataProvider invalidKeys
     */
    public function itThrowsExceptionOnInvalidKey(int $key): void
    {
        $this->expectException(OutOfRangeException::class);
        $intIntMap = new IntIntMap($this->shmop, shmop_size($this->shmop));
        $intIntMap->get($key);
    }

    public function invalidKeys(): Generator
    {
        $maxKey = intdiv(self::MEM_SIZE, PHP_INT_SIZE) / 2;

        yield [$maxKey + 1];
        yield [$maxKey + 10];
        yield [$maxKey + 100];
        yield [$maxKey + 1000];

        yield [-$maxKey - 1];
        yield [-$maxKey - 10];
        yield [-$maxKey - 100];
        yield [-$maxKey - 1000];
    }

    /**
     * @test
     * @dataProvider missingKeys
     */
    public function itGetsNullIfNoIndexFound(int $key): void
    {
        $intIntMap = new IntIntMap($this->shmop, shmop_size($this->shmop));
        static::assertNull($intIntMap->get($key));
    }

    public function missingKeys(): Generator
    {
        yield [0];
        yield [1];
        yield [100];
        yield [1000];
        yield [10000];
        yield [100000];
    }

    /**
     * @test
     * @dataProvider prevVals
     */
    public function itGetPrevValueCorrectly(int $key, int $firstVal, int $secondVal): void
    {
        $intIntMap = new IntIntMap($this->shmop, shmop_size($this->shmop));
        static::assertNull($intIntMap->put($key, $firstVal));
        static::assertSame($firstVal, $intIntMap->put($key, $secondVal));
    }

    public function prevVals(): Generator
    {
        yield [1, 100, 101];
        yield [2, 500, 1000];
        yield [3, -1000, -500];
        yield [1000, 101010, 101010];
        yield [2021, 100, 101];
        yield [2022, 0, 101];
    }
}
