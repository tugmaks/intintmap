FROM composer:2.0.9 as composer
FROM php:8.0.2-cli

ENV APP_DIR=/opt/app
ENV PATH=${APP_DIR}/bin:${APP_DIR}/vendor/bin:${PATH}

WORKDIR ${APP_DIR}

RUN apt-get update && apt-get install -y --no-install-recommends \
    git-core \
    unzip \
    libssl-dev \
    && docker-php-ext-install shmop

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_MEMORY_LIMIT -1
COPY --from=composer /usr/bin/composer /usr/bin/composer

COPY composer.json ${APP_DIR}/
RUN set -ex \
    && composer validate --no-check-lock \
    && composer install --ignore-platform-reqs --no-interaction --no-progress --no-scripts

ENV PHP_MEMORY_LIMIT 256M