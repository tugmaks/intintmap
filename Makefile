build:
	docker build . -t intintmap:latest
phpunit:
	docker-compose run app vendor/bin/phpunit
cli:
	docker-compose run app bash
php-cs-fixer:
	docker-compose run app php-cs-fixer fix
php-cs-fixer-dry:
	docker-compose run app php-cs-fixer fix --dry-run
phpstan:
	docker-compose run app vendor/bin/phpstan analyse src
psalm:
	docker-compose run app vendor/bin/psalm --show-info=true
check-code: phpstan psalm php-cs-fixer-dry