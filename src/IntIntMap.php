<?php
/**
 * @author Maxim Tugaev <tugmaks@yandex.ru>
 */

declare(strict_types=1);

namespace Tugmaks;

use OutOfRangeException;
use Shmop;
use function intdiv;
use function pack;
use function shmop_read;
use function shmop_write;
use function sprintf;
use function unpack;
use const PHP_INT_SIZE;

final class IntIntMap
{
    private int $minNegativeKey;
    private int $maxPositiveKey;
    private int $startOffset;

    public function __construct(
        private Shmop $shmop,
        private int $size
    ) {
        $mapSize = intdiv($this->size, PHP_INT_SIZE + 1);
        $this->maxPositiveKey = intdiv($mapSize, 2);
        $this->minNegativeKey = -intdiv($mapSize, 2);
        $this->startOffset = $mapSize + 1;
    }

    public function __destruct()
    {
        unset($this->shmop);
    }

    public function put(int $key, int $value): ?int
    {
        $prev = $this->get($key) ?? $this->getZeroByKey($key);

        if (0 === $value) {
            $this->putZeroByKey($key);
        } else {
            $binData = pack('q', $value);
            shmop_write($this->shmop, $binData, $this->calculateOffset($key));
        }

        return $prev;
    }

    public function get(int $key): ?int
    {
        $readData = shmop_read(
            $this->shmop,
            $this->calculateOffset($key),
            PHP_INT_SIZE
        );
        /** @phpstan-ignore-next-line */
        if (false === $readData) {
            return null;
        }
        /** @var array{int: int} $val */
        $val = unpack('qint', $readData);

        return 0 === $val['int'] ? null : $val['int'];
    }

    private function putZeroByKey(int $key): void
    {
        shmop_write($this->shmop, pack('a', '0'), $key);
    }

    /**
     * @see https://github.com/php/php-src/blob/master/ext/shmop/shmop.c#L243
     */
    private function calculateOffset(int $key): int
    {
        if ($key < $this->minNegativeKey || $key > $this->maxPositiveKey) {
            throw new OutOfRangeException(
                sprintf(
                    'Key should be in interval %d..%d, but %d provided',
                    $this->minNegativeKey,
                    $this->maxPositiveKey,
                    $key
                )
            );
        }

        return $this->startOffset
               + ($this->maxPositiveKey + $key) * PHP_INT_SIZE;
    }

    private function getZeroByKey(int $key): ?int
    {
        $readData = shmop_read(
            $this->shmop,
            $key,
            1
        );
        /** @phpstan-ignore-next-line */
        if (false === $readData) {
            return null;
        }
        /** @var array{zero: string} $val */
        $val = unpack('azero', $readData);

        return '0' === $val['zero'] ? 0 : null;
    }
}
