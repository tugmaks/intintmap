**Build**
```
make build
```

**Testing**
```
make phpunit
```

**Static analyzers and codestyle**
```
make check-code
```
