#!/usr/bin/env php
<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude('vendor')
    ;

return PhpCsFixer\Config::create()
    ->setRiskyAllowed(true)
    ->setRules([
        '@PhpCsFixer'                           => true,
        'array_syntax'                          => ['syntax' => 'short'],
        'binary_operator_spaces'                => ['operators' => ['=>' => 'single_space']],
        'fopen_flags'                           => ['b_mode' => true],
        'fully_qualified_strict_types'          => true,
        'global_namespace_import'               => ['import_classes' => true, 'import_constants' => true, 'import_functions' => true],
        'linebreak_after_opening_tag'           => true,
        'list_syntax'                           => ['syntax' => 'short'],
        'multiline_whitespace_before_semicolons'=> false,
        'native_function_invocation'            => true,
        'no_unreachable_default_argument_value' => true,
        'non_printable_character'               => false,
        'not_operator_with_space'               => false,
        'not_operator_with_successor_space'     => false,
        'no_unneeded_final_method'              => false,
        'ordered_class_elements'                => true,
        'ordered_imports'                       => ['sort_algorithm' => 'alpha','imports_order' => ['class', 'function', 'const']],
        'php_unit_internal_class'               => false,
        'php_unit_strict'                       => false,
        'php_unit_test_class_requires_covers'   => false,
        'phpdoc_to_comment'                     => false,
        'phpdoc_types_order'                    => ['null_adjustment' => 'always_last', 'sort_algorithm' => 'none'],
        'random_api_migration'                  => true,
        'simplified_null_return'                => false,
        'single_line_throw'                     => false,
        'void_return'                           => true,
        'php_unit_test_case_static_method_calls'=> true,
    ])
    ->setFinder($finder)
    ->setCacheFile(__DIR__.'/var/.php_cs.cache');
